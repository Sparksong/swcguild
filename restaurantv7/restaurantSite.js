	function nameBox() {
	if (document.getElementById('nameCheck').value == "") {
		alert('Name is required.')
		return false;
	}
	else {
		return true;
		}
	}

	function emailAndPhone() {
	var emailCheck = document.getElementById('emailCheck').value;
	var phoneCheck = document.getElementById('phoneCheck').value;
	if (emailCheck == "" && phoneCheck == "") {
		alert('Email Address or Phone Number is required.')
		return false;
	}
	else {
		return true;
		}
	}

	function otherSelect() {
	var options = document.getElementById('options').value;
	var moreInfo = document.getElementById('textarea').value;
	if (options == "other" && moreInfo == "") {
		alert('Other was selected; please give more information.');
		return false;
	}
	else {
		return true;
		}
	}
	
	function daysChecked() {
			if (document.getElementById("M").checked == false &&
				document.getElementById("T").checked == false &&
				document.getElementById("W").checked == false &&
				document.getElementById("Th").checked == false &&
				document.getElementById("F").checked == false) {
					alert("Please select at least one day to contact you.");
					return false;
				}
			else {
				return true;
			}
	}
