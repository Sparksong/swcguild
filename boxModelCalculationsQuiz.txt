Border (Bd),Padding(P),Margin(M),Content(C),Top(T),Bottom(B),Right(R),Left(L)
All units are px.

Total Height: 20(MT) + 20(MB) + 1(BdT) + 1(BdB) + 10(PT) + 10(PB) + 150(C) = 212px

Total Width: 20(MR) + 20(ML) + 1(BdR) + 1(BdL) + 10(PR) + 10(PL) + 400(C) = 462px

-

Browser Calculated Height: 1(BdT) + 1(BdB) + 10(PT) + 10(PB) + 150(C) = 172px

Browser Calculated Width: 1(BdR) + 1(BdL) + 10(PR) + 10(PL) + 400(C) = 422px

-

Margin is 20 px on all sides, border is 1 px on all sides and padding is 10 px on all sides.
Browder height and width have a 40 px difference from the Total height and width because margin is not included in calculations.

